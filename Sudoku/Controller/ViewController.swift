//
//  ViewController.swift
//  Sudoku
//
//  Created by David Christian on 01/02/21.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var labelTimer: UILabel!
    @IBOutlet weak var buttonPauseOrPlay: UIButton!
    @IBOutlet weak var collectionViewSudokuBoard: UICollectionView!
    
    var solver: SudokuSolver?
    var time = 300
    var isPaused = false
    var timer: Timer?
    var selectedItemIndex: Int?
    var selectedNumber: Int?
    var sudokuBoard: [[Int]]?
    var isAnswerWrong: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setUpBoard()
        loadSudoku()
    }
    
    func setUpBoard() {
        collectionViewSudokuBoard.delegate = self
        collectionViewSudokuBoard.dataSource = self
        
        collectionViewSudokuBoard.layer.backgroundColor = #colorLiteral(red: 0.1919676363, green: 0.2997605503, blue: 0.3631424904, alpha: 1)
        collectionViewSudokuBoard.layer.borderWidth = 3
        collectionViewSudokuBoard.layer.borderColor = #colorLiteral(red: 0.8808811307, green: 0.4774965048, blue: 0.2528152466, alpha: 1)
        collectionViewSudokuBoard.layer.cornerRadius = 10
    }
    
    func loadSudoku() {
        time = 300
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
        buttonPauseOrPlay.setImage(UIImage(named: "buttonPause"), for: .normal)
        let randomCase = Int.random(in: 1...4)
        
        switch randomCase{
        case 1:
            solver = SudokuSolver(sudokuBoard: [[7,8,1,5,4,3,9,2,6],
                                                [0,0,6,1,7,9,5,0,0],
                                                [9,5,4,6,2,8,7,3,1],
                                                [6,9,5,8,3,7,2,1,4],
                                                [1,4,8,2,6,5,3,7,9],
                                                [3,2,7,9,1,4,8,0,0],
                                                [4,1,3,7,5,2,6,9,8],
                                                [0,0,2,0,0,0,4,0,0],
                                                [5,7,9,4,8,6,1,0,3]])
        case 2:
            solver = SudokuSolver(sudokuBoard: [[3, 0, 6, 5, 0, 8, 4, 0, 0],
                                                [5, 2, 0, 0, 0, 0, 0, 0, 0],
                                                [0, 8, 7, 0, 0, 0, 0, 3, 1],
                                                [0, 0, 3, 0, 1, 0, 0, 8, 0],
                                                [9, 0, 0, 8, 6, 3, 0, 0, 5],
                                                [0, 5, 0, 0, 9, 0, 6, 0, 0],
                                                [1, 3, 0, 0, 0, 0, 2, 5, 0],
                                                [0, 0, 0, 0, 0, 0, 0, 7, 4],
                                                [0, 0, 5, 2, 0, 6, 3, 0, 0]])
        case 3:
            solver = SudokuSolver(sudokuBoard: [[1, 7, 0, 0, 0, 0, 0, 0, 6],
                                                [0, 4, 0, 1, 0, 6, 0, 0, 0],
                                                [0, 0, 0, 0, 0, 5, 2, 0, 8],
                                                [4, 0, 0, 0, 7, 8, 0, 0, 0],
                                                [0, 0, 6, 0, 0, 0, 0, 0, 5],
                                                [0, 0, 0, 0, 0, 1, 3, 0, 0],
                                                [0, 2, 0, 9, 0, 4, 5, 0, 0],
                                                [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                                [8, 1, 0, 0, 0, 0, 6, 4, 9]])
        case 4:
            solver = SudokuSolver(sudokuBoard: [[0, 0, 1, 4, 0, 0, 0, 7, 0],
                                                [6, 0, 0, 7, 0, 0, 0, 0, 8],
                                                [0, 0, 0, 0, 0, 1, 9, 0, 0],
                                                [0, 1, 0, 0, 0, 0, 0, 0, 3],
                                                [4, 0, 0, 2, 8, 0, 0, 0, 0],
                                                [0, 0, 3, 6, 0, 9, 0, 0, 0],
                                                [0, 0, 0, 0, 0, 0, 5, 0, 4],
                                                [0, 8, 0, 0, 0, 2, 3, 0, 0],
                                                [0, 7, 4, 0, 0, 0, 0, 0, 0]])
        default:
            break
        }
        
        sudokuBoard = solver?.board
    }
    
    func timerFormater() -> String{
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
    }
    
    @IBAction func buttonNewGame(_ sender: Any) {
        timer?.invalidate()
        loadSudoku()
        collectionViewSudokuBoard.reloadData()
    }
    
    @IBAction func buttonPause(_ sender: Any) {
        if isPaused {
            buttonPauseOrPlay.setImage(UIImage(named: "buttonPause"), for: .normal)
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
            isPaused = false
        }
        else {
            buttonPauseOrPlay.setImage(UIImage(named: "buttonPlay"), for: .normal)
            timer?.invalidate()
            isPaused = true
        }
    }
    
    @IBAction func buttonSolve(_ sender: Any) {
        let isSuccess = solver?.solve()
        timer?.invalidate()
        buttonPauseOrPlay.setImage(UIImage(named: "buttonPlay"), for: .normal)
        
        if let success = isSuccess {
            for i in 0..<9  {
                for j in 0..<9 {
                    if solver?.board[i][j] != sudokuBoard?[i][j] {
                        isAnswerWrong = true
                        break
                    }
                }
            }
            
            if !success {
                let alert = UIAlertController(title: "Soal Salah", message: "Maaf soal yang diberikan salah, silahkan ulangi lagi!", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ulangi", style: .default, handler: { _ in
                    self.loadSudoku()
                    self.collectionViewSudokuBoard.reloadData()
                    alert.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
            }
            
            if isAnswerWrong {
                sudokuBoard = solver?.board
                collectionViewSudokuBoard.reloadData()
                
                let alert = UIAlertController(title: "Jawaban Salah", message: "Maaf jawaban yang diberikan salah, silahkan ulangi lagi!", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ulangi", style: .default, handler: { _ in
                    self.loadSudoku()
                    self.collectionViewSudokuBoard.reloadData()
                    alert.dismiss(animated: true, completion: nil)
                }))
                alert.addAction(UIAlertAction(title: "Lihat Jawaban", style: .default, handler: { _ in
                    alert.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
            }
            
            else {
                let alert = UIAlertController(title: "Jawaban Benar", message: "Selamat! Anda sudah menjawab dengan benar!", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Main Lagi", style: .default, handler: { _ in
                    self.loadSudoku()
                    self.collectionViewSudokuBoard.reloadData()
                    alert.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
            }
            
        }
        else {
            print("fail")
        }
    }
    
    @objc func updateCounter() {
        if time >= 0 {
            let formatedTime = timerFormater()
            labelTimer.text = formatedTime
            time -= 1
        }
        else {
            timer?.invalidate()
            let alert = UIAlertController(title: "Waktu Habis", message: "Waktu untuk menyelesaikan sudoku sudah habis, silahkan coba lagi!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ulangi", style: .default, handler: { _ in
                self.time = 300
                self.isPaused = true
                self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateCounter), userInfo: nil, repeats: true)
                alert.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK:- Untuk Action Memilih Angka
    
    @IBAction func buttonOne(_ sender: Any) {
        changeNumberValue(num: 1)
        collectionViewSudokuBoard.reloadData()
    }
    
    @IBAction func buttonTwo(_ sender: Any) {
        changeNumberValue(num: 2)
        collectionViewSudokuBoard.reloadData()
    }
    
    @IBAction func buttonThree(_ sender: Any) {
        changeNumberValue(num: 3)
        collectionViewSudokuBoard.reloadData()
    }
    
    @IBAction func buttonFour(_ sender: Any) {
        changeNumberValue(num: 4)
        collectionViewSudokuBoard.reloadData()
    }
    
    @IBAction func buttonFive(_ sender: Any) {
        changeNumberValue(num: 5)
        collectionViewSudokuBoard.reloadData()
    }
    
    @IBAction func buttonSix(_ sender: Any) {
        changeNumberValue(num: 6)
        collectionViewSudokuBoard.reloadData()
    }
    
    @IBAction func buttonSeven(_ sender: Any) {
        changeNumberValue(num: 7)
        collectionViewSudokuBoard.reloadData()
    }
    
    @IBAction func buttonEight(_ sender: Any) {
        changeNumberValue(num: 8)
        collectionViewSudokuBoard.reloadData()
    }
    
    @IBAction func buttonNine(_ sender: Any) {
        changeNumberValue(num: 9)
        collectionViewSudokuBoard.reloadData()
    }
    
    //MARK: - Untuk Mengganti isi dari board
    func changeNumberValue(num: Int) {
        if let index = selectedItemIndex {
            let row: Double = (Double(index) + 1.0) / 9.0
            let rowLocation = (row.rounded(.up)) - 1
            
            let col = (index + 1) % 9
            var colLocation = col - 1
            
            if col == 0 && index > 1 {
                colLocation = 8
            }
            sudokuBoard?[Int(rowLocation)][colLocation] = num
        }
    }
}

extension ViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 81
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionViewSudokuBoard.dequeueReusableCell(withReuseIdentifier: "Box", for: indexPath) as! BoxCollectionViewCell
        
        let row: Double = (Double(indexPath.row) + 1.0) / 9.0
        let rowLocation = (row.rounded(.up)) - 1
        
        let col = (indexPath.row + 1) % 9
        var colLocation = col - 1
        
        if col == 0 && indexPath.row > 1 {
            colLocation = 8
        }
        
        cell.layer.removeBorder()
        
        //setup board number
        if let num = sudokuBoard?[Int(rowLocation)][colLocation] {
            if num != 0 {
                cell.labelNumber.text = "\(num)"
            }
            else {
                cell.labelNumber.text = ""
            }
        }
        
        //mendefine kotak yang bisa dipilih
        if let num = solver?.board[Int(rowLocation)][colLocation]{
            if num != 0 {
                cell.isUserInteractionEnabled = false
            }
            else {
                cell.isUserInteractionEnabled = true
            }
        }
        
        //warna kotak-kotak
        if indexPath.row % 2 == 0 {
            cell.layer.backgroundColor = #colorLiteral(red: 0.1607843137, green: 0.2031657316, blue: 0.3018367015, alpha: 1)
        }
        else {
            cell.layer.backgroundColor = #colorLiteral(red: 0.1919676363, green: 0.2997605503, blue: 0.3631424904, alpha: 1)
        }
        
        //warna border
        if let index = selectedItemIndex {
            if indexPath.row == index {
                cell.layer.borderColor = #colorLiteral(red: 0.8808811307, green: 0.4774965048, blue: 0.2528152466, alpha: 1)
                cell.layer.borderWidth = 3
                
            }
            else {
                cell.layer.borderWidth = 0
            }
        }
        
        //warna sekat
        if rowLocation == 2 || rowLocation == 5 {
            cell.layer.addBorder(edge: .bottom, color: #colorLiteral(red: 0.8808811307, green: 0.4774965048, blue: 0.2528152466, alpha: 1), thickness: 3)
        }
        
        if colLocation == 2 || colLocation == 5 {
            cell.layer.addBorder(edge: .right, color: #colorLiteral(red: 0.8808811307, green: 0.4774965048, blue: 0.2528152466, alpha: 1), thickness: 3)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width/9, height: collectionView.frame.size.width/9)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if !isPaused {
            selectedItemIndex = indexPath.row
            collectionView.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
