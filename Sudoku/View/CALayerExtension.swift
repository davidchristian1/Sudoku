//
//  CALayerExtension.swift
//  Sudoku
//
//  Created by David Christian on 01/02/21.
//

import Foundation
import UIKit

extension CALayer {
    
    func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat) {
        
        let border = CALayer()
        
        border.name = "addedBorder"
        
        switch edge {
        case .bottom:
            border.frame = CGRect(x: 0, y: frame.height - thickness, width: frame.width, height: thickness)
        case .right:
            border.frame = CGRect(x: frame.width - thickness, y: 0, width: thickness, height: frame.height)
        default:
            break
        }
        border.backgroundColor = color.cgColor
        addSublayer(border)
    }
    
    func removeBorder() {
        for layer in sublayers ?? [] {
             if layer.name == "addedBorder" {
                  layer.removeFromSuperlayer()
             }
         }
    }
}


