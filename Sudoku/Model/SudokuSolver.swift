//
//  SudokuSolver.swift
//  Sudoku
//
//  Created by David Christian on 01/02/21.
//

import Foundation

class SudokuSolver {
    var board: [[Int]]
    
    init(sudokuBoard: [[Int]]) {
        board = sudokuBoard
    }
    
    /// Check apakah angka yang akan di assign sudah terdapat dalam row tersebut
    func checkRow(num: Int, rowPosition: Int) -> Bool {
        for i in 0..<board.count {
            if board[rowPosition][i] == num {
                return false
            }
        }
        return true
    }
    
    /// Check apakah angka yang akan di assign sudah terdapat dalam kolom tersebut
    func checkCol(num: Int, colPosition: Int) -> Bool {
        for i in 0..<board.count {
            if board[i][colPosition] == num {
                return false
            }
        }
        return true
    }
    
    /// Check apakah angka yang akan di assign sudah terdapat dalam matrix 3x3
    func checkMatrix(num: Int, rowPosition: Int, colPosition: Int) -> Bool {
        let startIndexMatrix = checkPosition(rowPosition: rowPosition, colPosition: colPosition)
        let startIndexRow = startIndexMatrix.0
        let startIndexCol = startIndexMatrix.1
        
        for i in startIndexRow...startIndexRow+2 {
            for j in startIndexCol...startIndexCol+2 {
                if board[i][j] == num {
                    return false
                }
            }
        }
        
        return true
    }
    
    /// Check posisi kotak yang mau diisi angka terletak pada matrix yang mana. Return start index matrix di kiri atas
    func checkPosition(rowPosition: Int, colPosition: Int) -> (Int,Int) {
        let boxRow: Double = Double(rowPosition+1)/3.0
        let boxCol: Double = Double(colPosition+1)/3.0
        
        if (boxRow <= 1 && boxCol <= 1) {
            return (0,0)
        }
        else if (boxRow <= 2 && boxRow > 1 && boxCol <= 1) {
            return (3,0)
        }
        else if (boxRow <= 3 && boxRow > 2 && boxCol <= 1) {
            return (6,0)
        }
        else if (boxRow <= 1 && boxCol <= 2 && boxCol > 1) {
            return (0,3)
        }
        else if (boxRow <= 2 && boxRow > 1 && boxCol <= 2 && boxCol > 1) {
            return (3,3)
        }
        else if (boxRow <= 3 && boxRow > 2 && boxCol <= 2 && boxCol > 1) {
            return (6,3)
        }
        else if (boxRow <= 1 && boxCol <= 3 && boxCol > 2) {
            return (0,6)
        }
        else if (boxRow <= 2 && boxRow > 1 && boxCol <= 3 && boxCol > 2) {
            return (3,6)
        }
        else if (boxRow <= 3 && boxRow > 2 && boxCol <= 3 && boxCol > 2) {
            return (6,6)
        }
        return (0,0)
    }
    
    ///Fungsi untuk Menyelesaikan Sudoku
    func solve() -> Bool {
        var isEmpty = true
        var row = -1
        var col = -1
        
        for i in 0..<board.count {
            if board[i].contains(0) {
                //cek apakah ada yang masih belum terisi di sudoku boardnya.
                row = i
                col = board[i].firstIndex(of: 0)!
                
                isEmpty = false
                break
            }
        }
        
        if isEmpty {
            //sudah tersolve semua
            return true
        }
        
        for num in 1...board.count {
            if (checkRow(num: num, rowPosition: row) && checkCol(num: num, colPosition: col) && checkMatrix(num: num, rowPosition: row, colPosition: col)) {
                board[row][col] = num
                
                if (solve()) {
                    return true
                }
                else {
                    board[row][col] = 0
                }
            }
        }
        return false
    }
}
