//
//  SudokuTests.swift
//  SudokuTests
//
//  Created by David Christian on 01/02/21.
//

import XCTest
@testable import Sudoku

class SudokuTests: XCTestCase {
    
    let board = SudokuSolver(sudokuBoard: [[3, 0, 6, 5, 0, 8, 4, 0, 0],
                                           [5, 2, 0, 0, 0, 0, 0, 0, 0],
                                           [0, 8, 7, 0, 0, 0, 0, 3, 1],
                                           [0, 0, 3, 0, 1, 0, 0, 8, 0],
                                           [9, 0, 0, 8, 6, 3, 0, 0, 5],
                                           [0, 5, 0, 0, 9, 0, 6, 0, 0],
                                           [1, 3, 0, 0, 0, 0, 2, 5, 0],
                                           [0, 0, 0, 0, 0, 0, 0, 7, 4],
                                           [0, 0, 5, 2, 0, 6, 3, 0, 0]])

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

    func testCheckRowFalse() throws {
        let checkRowFalse = board.checkRow(num: 3, rowPosition: 0)
        XCTAssertFalse(checkRowFalse)
    }
    
    func testCheckRowTrue() throws {
        let checkRowTrue = board.checkRow(num: 1, rowPosition: 0)
        XCTAssertTrue(checkRowTrue)
    }
    
    func testCheckColFalse() throws {
        let checkColFalse = board.checkCol(num: 2, colPosition: 1)
        XCTAssertFalse(checkColFalse)
    }
    
    func testCheckColTrue() throws {
        let checkColTrue = board.checkCol(num: 1, colPosition: 1)
        XCTAssertTrue(checkColTrue)
    }
    
    func testCheckMatrixFalse() throws {
        let checkMatrixFalse = board.checkMatrix(num: 3, rowPosition: 1, colPosition: 1)
        XCTAssertFalse(checkMatrixFalse)
    }
    
    func testCheckMatrixTrue() throws {
        let checkMatrixTrue = board.checkMatrix(num: 1, rowPosition: 1, colPosition: 1)
        XCTAssertTrue(checkMatrixTrue)
    }
    
    func testCheckPosition() throws {
        var checkPosition = board.checkPosition(rowPosition: 2, colPosition: 1)
        XCTAssertTrue(checkPosition == (0,0))
        
        checkPosition = board.checkPosition(rowPosition: 5, colPosition: 4)
        XCTAssertTrue(checkPosition == (3,3))
    }
    
    func testSolve() throws {
        let solve = board.solve()
        XCTAssertTrue(solve)
    }
    
}
